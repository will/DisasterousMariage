#include <DisasterousMariage/AnalysisUtils.h>
#include <DisasterousMariage/SUSYAnalysisHelper.h>

#include <PATInterfaces/SystematicSet.h>
#include <PathResolver/PathResolver.h>

#include <SUSYTools/SUSYObjDef_xAOD.h>

#ifndef XAOD_STANDALONE
#include <GaudiKernel/ITHistSvc.h>
#endif

#define SET_DUAL_TOOL( TOOLHANDLE, TOOLTYPE, TOOLNAME )                \
  ASG_SET_ANA_TOOL_TYPE(TOOLHANDLE, TOOLTYPE);                        \
  TOOLHANDLE.setName(TOOLNAME);

#include <iostream>
#include <fstream>

namespace MariageOfDisaster {
    SUSYAnalysisHelper::SUSYAnalysisHelper(std::string myname) :
                AsgTool(myname),                
                m_susytools("SUSYTools"),
                m_GoodRunsListVec(),
                m_PRWConfigFiles(),
                m_PRWLumiCalcFiles(),
#ifdef XAOD_STANDALONE
                m_GoodRunsListVec_string(""),
                m_PRWConfigFiles_string(""),
                m_PRWLumiCalcFiles_string("")
#endif
                 {

        //Tool properties
        m_susytools.declarePropertyFor(this, "SUSYTools", "The SUSYTools instance");       
      
     
#ifdef XAOD_STANDALONE
        declareProperty("GoodRunsListVecString", m_GoodRunsListVec_string = "");
        declareProperty("PRWConfigFilesString", m_PRWConfigFiles_string = "");
        declareProperty("PRWLumiCalcFilesString", m_PRWLumiCalcFiles_string = "");
#endif
        // SUSYTools properties and settings
        declareProperty("STConfigFile", m_STConfigFile = "SUSYTools/SUSYTools_Default.conf");
        declareProperty("GoodRunsLists", m_GoodRunsListVec);
        declareProperty("PRWConfigFiles", m_PRWConfigFiles);
        declareProperty("PRWLumiCalcFiles", m_PRWLumiCalcFiles);
    }
    SUSYAnalysisHelper::~SUSYAnalysisHelper() {      
                ATH_MSG_INFO("Destructor called");
#ifdef XAOD_STANDALONE
        //It seems that PyROOT and smart pointers engage a marigage of deseaster
        //The basic problem is that the smart pointer and PyROOT itself are trying to delete the object each
        //The problem can be solved via leading the concept of smart pointers ad absurdum. Have fun with it

//        asg::AnaToolHandle<ST::ISUSYObjDef_xAODTool>* MyCrap = new asg::AnaToolHandle<ST::ISUSYObjDef_xAODTool>("");
//        MyCrap->swap(m_susytools);
#endif

    }
    StatusCode SUSYAnalysisHelper::initializeSUSYTools() {
#ifdef XAOD_STANDALONE
        MariageOfDisaster::FillVectorFromString(m_PRWConfigFiles, m_PRWConfigFiles_string);
        MariageOfDisaster::FillVectorFromString(m_PRWLumiCalcFiles, m_PRWLumiCalcFiles_string);
#endif
        if (!m_susytools.isUserConfigured()) {
            ATH_MSG_INFO("Setup new instance of SUSYTools");
            SET_DUAL_TOOL(m_susytools, ST::SUSYObjDef_xAOD, "SUSYTools");
            ATH_CHECK(m_susytools.setProperty("ConfigFile", PathResolverFindCalibFile(m_STConfigFile)));
            ST::ISUSYObjDef_xAODTool::DataSource datasource = ST::ISUSYObjDef_xAODTool::DataSource::FullSim;
            ATH_CHECK(m_susytools.setProperty("PRWConfigFiles", GetPathResolvedFileList(m_PRWConfigFiles)));
            ATH_CHECK(m_susytools.setProperty("PRWLumiCalcFiles", GetPathResolvedFileList(m_PRWLumiCalcFiles)));
            if (m_PRWConfigFiles.empty() || m_PRWLumiCalcFiles.empty()) {
                ATH_MSG_WARNING("Cannot setup prwTool properly. Switch off prw");              
            }
#ifdef XAOD_STANDALONE

            ATH_CHECK(m_susytools.setProperty("DataSource" ,datasource));
#else
            ATH_CHECK(m_susytools.setProperty("DataSource", (int) datasource));

#endif
            ATH_CHECK(m_susytools.initialize());
            //This line seems to initialize SUSYTools in RootCore tool

        } else {
            ATH_CHECK(m_susytools.retrieve());
        }
        m_PRWConfigFiles.clear();
        m_PRWLumiCalcFiles.clear();
        return StatusCode::SUCCESS;
    }
    StatusCode SUSYAnalysisHelper::initializeAnalysisTools() {
        ATH_MSG_INFO("Starting Analysis Setup");
        ATH_CHECK(initializeSUSYTools());
        return StatusCode::SUCCESS;
    }

    StatusCode SUSYAnalysisHelper::initialize() {
        ATH_CHECK(initializeAnalysisTools());
        return StatusCode::SUCCESS;
    }
   
    StatusCode SUSYAnalysisHelper::finalize() {
        return StatusCode::SUCCESS;
    } 
    ST::SUSYObjDef_xAOD* SUSYAnalysisHelper::SUSYToolsPtr() {
        ST::SUSYObjDef_xAOD* ST = dynamic_cast<ST::SUSYObjDef_xAOD*>(m_susytools.operator->());
        return ST;
    }
   
}
