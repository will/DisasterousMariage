#include <DisasterousMariage/AnalysisUtils.h>

#include <xAODEgamma/ElectronxAODHelpers.h>
#include <xAODEgamma/EgammaTruthxAODHelpers.h>
#include <xAODBase/IParticleHelpers.h>
#include <PathResolver/PathResolver.h>

namespace MariageOfDisaster {
    std::string RemoveAllExpInStr(std::string Str, std::string Exp) {
        size_t Pos = Str.find(Exp);
        size_t sSize = Str.size();
        size_t eSize = Exp.size();
        if (Pos < sSize) {
            std::string NewStr = Str.substr(0, Pos);
            if (Pos + eSize + 2 < sSize) NewStr += Str.substr(Pos + eSize + 1, sSize);
            return RemoveAllExpInStr(NewStr, Exp);
        }
        return Str;
    }
    std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep) {
        size_t ExpPos = str.find(exp);
        if (ExpPos == std::string::npos) return str;
        size_t ExpLen = exp.size();

        str = str.substr(0, ExpPos) + rep + str.substr(ExpPos + ExpLen, std::string::npos);
        if (str.find(exp) != std::string::npos) return ReplaceExpInString(str, exp, rep);
        return str;
    }
    std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &FileList) {
        std::vector < std::string > ResolvedList;
        for (auto& File : FileList){
            std::string F = PathResolverFindCalibFile(File);
            Info("GetPathResolvedFileList()", "Add file %s", F.c_str());
            ResolvedList.push_back(F);
        }
        return ResolvedList;
    }

   void FillVectorFromString(std::vector<std::string> & str_vector, std::string &str) {
        if (str.find(",") < str.size()) {
            str = str + ","; // adding comma in the end to make the 'while' working for the last element
            while (str.find(",") < str.size()) {
                std::size_t Pos = str.find(",");
                std::string Data = str.substr(0, Pos);
                if (!Data.empty() && !IsInVector(Data, str_vector)) str_vector.push_back(Data);
                str = str.substr(Pos + 1, str.size());
            }
        } else if (!str.empty() && !IsInVector(str, str_vector)) str_vector.push_back(str);
        str.clear();
    }
    bool GetLine(std::ifstream &inf, std::string &line) {
        if (!std::getline(inf, line)) return false;
        line = EraseWhiteSpaces(line);
        if (line.find("#") == 0 || line.size() < 1) return GetLine(inf, line);
        return true;
    }
    std::string EraseWhiteSpaces(std::string str) {
        str.erase(std::remove(str.begin(), str.end(), '\t'), str.end());
        if (str.find(" ") == 0) return EraseWhiteSpaces(str.substr(1, str.size()));
        if (str.size() > 0 && str.find(" ") == str.size() - 1) return EraseWhiteSpaces(str.substr(0, str.size() - 1));
        return str;
    }
    MSG::Level setOutputLevel(int m_output_level_int) {
        if (m_output_level_int == 1) return MSG::VERBOSE;
        if (m_output_level_int == 2) return MSG::DEBUG;
        if (m_output_level_int == 3) return MSG::INFO;
        if (m_output_level_int == 4) return MSG::WARNING;
        if (m_output_level_int == 5) return MSG::ERROR;
        if (m_output_level_int == 6) return MSG::FATAL;
        return MSG::INFO;
    }
    void PromptParticle(const xAOD::IParticle* Part, std::string AddInfo) {
        if (Part == NULL) {
            Warning("PromptParticle()", "Null ptr given");
            return;
        }
        std::string PartType;
        if (Part->type() == xAOD::Type::ObjectType::Electron) PartType = "Electron";
        else if (Part->type() == xAOD::Type::ObjectType::Muon) PartType = "Muon";
        else if (Part->type() == xAOD::Type::ObjectType::Tau) PartType = "Tau";
        else if (Part->type() == xAOD::Type::ObjectType::Jet) PartType = "Jet";
        else if (Part->type() == xAOD::Type::ObjectType::Photon) PartType = "Photon";
        else if (Part->type() == xAOD::Type::ObjectType::TrackParticle) PartType = "Track";
        else if (Part->type() == xAOD::Type::ObjectType::CaloCluster) PartType = "CaloCluster";
        else if (Part->type() == xAOD::Type::ObjectType::TruthParticle) {
            PartType = "Truth_";
            const xAOD::TruthParticle *Tpart = dynamic_cast<const xAOD::TruthParticle*>(Part);
            if (Tpart != NULL) {
                if (Tpart->isElectron() == true) PartType += "Electron";
                else if (Tpart->isMuon() == true) PartType += "Muon";
                else if (Tpart->isTau() == true) PartType += "Tau";
                else if (Tpart->isW() == true) PartType += "W-Boson";
                else if (Tpart->isZ() == true) PartType += "Z-Boson";
                else if (Tpart->isNeutrino() == true) PartType += "Neutrino";
                else if (Tpart->isPhoton() == true) PartType += "Photon";
                else if (Tpart->isHiggs() == true) PartType += "Higgs";
                else if (Tpart->absPdgId() == 1) PartType += "Down";
                else if (Tpart->absPdgId() == 2) PartType += "Up";
                else if (Tpart->absPdgId() == 3) PartType += "Strange";
                else if (Tpart->absPdgId() == 4) PartType += "Charm";
                else if (Tpart->absPdgId() == 5) PartType += "Bottom";
                else if (Tpart->absPdgId() == 6) PartType += "Top";
                else if (Tpart->absPdgId() == 21) PartType += "Gluon";
                else if (Tpart->absPdgId() == 2212) PartType += "Proton";
                else if (Tpart->absPdgId() == 1000001 || Tpart->absPdgId() == 2000001) PartType += "Sdown";
                else if (Tpart->absPdgId() == 1000002 || Tpart->absPdgId() == 2000002) PartType += "Sup";
                else if (Tpart->absPdgId() == 1000003 || Tpart->absPdgId() == 2000003) PartType += "Sstrange";
                else if (Tpart->absPdgId() == 1000004 || Tpart->absPdgId() == 2000004) PartType += "Scharm";
                else if (Tpart->absPdgId() == 1000005 || Tpart->absPdgId() == 2000005) PartType += "Sbottom";
                else if (Tpart->absPdgId() == 1000006 || Tpart->absPdgId() == 2000006) PartType += "Stop";
                else if (Tpart->absPdgId() == 1000021) PartType += "Gluino";
                else if (Tpart->absPdgId() == 1000022) PartType += "Bino";
                else if (Tpart->absPdgId() == 1000023) PartType += "Zino";
                else if (Tpart->absPdgId() == 1000024) PartType += "Wino";
                AddInfo = Form("pdgId: %i   Barcode: %i Status: %i  %s", Tpart->pdgId(), Tpart->barcode(), Tpart->status(), AddInfo.c_str());
            }
        }
        Info(PartType.c_str(), Form("m: %.2f GeV    pt: %.2f GeV    eta: %.4f   phi: %.4f   %s", Part->p4().M() / 1000., (Part->pt() / 1000.), Part->eta(), Part->phi(), AddInfo.c_str()));
    }
}
