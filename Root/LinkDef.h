#ifndef DisasterousMariage_LINKDEF_H
#define DisasterousMariage_LINKDEF_H
#include <DisasterousMariage/SUSYAnalysisHelper.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ namespace MariageOfDisaster;
#pragma link C++ class MariageOfDisaster::SUSYAnalysisHelper;
#endif

#endif
