// XAMPPbase includes
#include "XAMPPalgorithm.h"
#include <XAMPPbase/xAODLoop.h>

#include <XAMPPbase/IAnalysisHelper.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ISystematics.h>

namespace XAMPP {
    XAMPPalgorithm::XAMPPalgorithm(const std::string& name, ISvcLocator* pSvcLocator) :
                AthAlgorithm(name, pSvcLocator),
                m_systematics("SystematicsTool"),
                m_helper("AnalysisHelper"),
                m_RunCutFlow(true),
                m_init(false){
        declareProperty("AnalysisHelper", m_helper);
        declareProperty("SystematicsTool", m_systematics);
        declareProperty("RunCutFlow", m_RunCutFlow);
    }

    XAMPPalgorithm::~XAMPPalgorithm() {
    }

    StatusCode XAMPPalgorithm::initialize() {
        if (m_init){
            return StatusCode::SUCCESS;
        }
        ATH_MSG_INFO("Initializing " << name() << "...");
        ATH_MSG_DEBUG("Initialize the analysis helper class");
        ATH_CHECK(m_helper.retrieve());
        ATH_CHECK(m_systematics.retrieve());
        ATH_MSG_DEBUG("The analysis helper must be initialized by the loop, it then will initialize the systematics tool.");
        ATH_CHECK(m_helper->initialize());
        if (m_systematics->GetKinematicSystematics().empty()) {
            ATH_MSG_FATAL("Have not found any iteration to run on, exiting...!");
            return StatusCode::FAILURE;
        }
        m_init = true;
        return StatusCode::SUCCESS;
    }

    StatusCode XAMPPalgorithm::finalize() {
        ATH_MSG_INFO("Finalizing " << name() << "...");
        CHECK(m_helper->finalize());
        return StatusCode::SUCCESS;
    }
    StatusCode XAMPPalgorithm::execute() {
        ATH_MSG_DEBUG("Executing " << name() << "...");
        if (!m_init){
            ATH_MSG_ERROR("Algorithm not initialized");
            return StatusCode::FAILURE;
        }
        CHECK(ExecuteEvent());
        if (m_RunCutFlow) {
            CHECK(CheckCutflow());
        }
        return StatusCode::SUCCESS;
    }

    StatusCode XAMPPalgorithm::ExecuteEvent() {
        ATH_MSG_DEBUG("Call beginEvent...");
        ATH_CHECK(m_helper->LoadContainers());
        ATH_MSG_DEBUG("ExecuteEvent()....");
        if (!m_helper->AcceptEvent()) {
            ATH_MSG_DEBUG("The event is discarded by the AnalysisHelper");
            return StatusCode::SUCCESS;
        }

        ATH_MSG_DEBUG("Check event cleaning...");
        if (!m_helper->EventCleaning()) {
            ATH_MSG_DEBUG("Event Failed the cleaning");
            return StatusCode::SUCCESS;
        }
        ATH_MSG_DEBUG("Check trigger...");
        if (!m_helper->CheckTrigger()) {
            ATH_MSG_DEBUG("Trigger failed");
            return StatusCode::SUCCESS;
        }
        for (const auto& current_syst : m_systematics->GetKinematicSystematics()) {
            ATH_MSG_DEBUG("Running kinematic systematic: " << current_syst->name() << ".");
            ATH_CHECK(m_systematics->resetSystematics());
            ATH_CHECK(m_systematics->setSystematic(current_syst));
            ATH_MSG_DEBUG("FillInitialObjects: ");
            ATH_CHECK(m_helper->FillInitialObjects(current_syst));
            ATH_MSG_DEBUG("RemoveOverlap: ");
            ATH_CHECK(m_helper->RemoveOverlap());
            ATH_MSG_DEBUG("FillObjects: ");
            ATH_CHECK(m_helper->FillObjects(current_syst));
            ATH_MSG_DEBUG("CleanObjects?");
            if (!m_helper->CleanObjects(current_syst)) {
                ATH_MSG_DEBUG("Found bad objects in the current systematic" << current_syst->name());
                continue;
            }
            ATH_MSG_DEBUG("Call FillEvent");
            ATH_CHECK(m_helper->FillEvent(current_syst));
        }
        ATH_MSG_DEBUG("Call DumpCxAOD");
        ATH_CHECK(m_helper->DumpCxAOD());
        return StatusCode::SUCCESS;
    }
    StatusCode XAMPPalgorithm::CheckCutflow() {
        if (!m_RunCutFlow) return StatusCode::SUCCESS;
        for (const auto& current_syst : m_systematics->GetKinematicSystematics()) {
            ATH_CHECK(m_helper->CheckCutFlow(current_syst));
        }
        return StatusCode::SUCCESS;
    }

}

