#ifndef DisasterousMariage_Defs_H
#define DisasterousMariage_Defs_H

namespace MariageOfDisaster {
    enum RunType {
        Local = 0, Grid, Batch
    };
    enum SelectionObject {
        Other = 0, Jet = 2, Electron = 6, Photon = 7, Muon = 8, Tau = 9, BTag = 102, TruthParticle = 201, MissingET = 301, EventWeight = 302, RecoParticle = 960, DiTau = 980
    };
  }
#endif
