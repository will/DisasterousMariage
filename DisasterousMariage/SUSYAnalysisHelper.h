#ifndef DisasterousMariage_SUSYAnalysisHelper_H
#define DisasterousMariage_SUSYAnalysisHelper_H

#include <DisasterousMariage/IAnalysisHelper.h>

#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>


#include <SUSYTools/SUSYObjDef_xAOD.h>
#include "TFile.h"

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iomanip>

#ifndef XAOD_STANDALONE
class ITHistSvc;
#endif

namespace ST {
    class SUSYObjDef_xAOD;
    class ISUSYObjDef_xAODTool;
}
namespace SUSY {
    class CrossSectionDB;
}
namespace CP {
    class SystematicSet;
    class IJetTileCorrectionTool;
}
class IGoodRunsListSelectionTool;

namespace MariageOfDisaster {
    class ISystematics;

    class SUSYAnalysisHelper: public asg::AsgTool, virtual public IAnalysisHelper {
        public:
            // Create a proper constructor for Athena
            ASG_TOOL_CLASS(SUSYAnalysisHelper, MariageOfDisaster::IAnalysisHelper)

            SUSYAnalysisHelper(std::string myname);
            virtual ~SUSYAnalysisHelper();

            virtual StatusCode initialize();
            virtual StatusCode finalize();      

        protected:
       

            //These functions should only be overwritten if there are additional tools needed to  be initialized
            virtual StatusCode initializeAnalysisTools();
            virtual StatusCode initializeSUSYTools();
            asg::AnaToolHandle<ST::ISUSYObjDef_xAODTool> m_susytools;

            //Retrieve the SUSYTools pointer from the Tool Handle
            ST::SUSYObjDef_xAOD* SUSYToolsPtr();

        private:
            // SUSYTools properties and settings
            std::string m_STConfigFile;           
            std::vector<std::string> m_GoodRunsListVec;
            std::vector<std::string> m_PRWConfigFiles;
            std::vector<std::string> m_PRWLumiCalcFiles;
            // also set single string as property for RC python config
#ifdef XAOD_STANDALONE
            std::string m_GoodRunsListVec_string;
            std::string m_PRWConfigFiles_string;
            std::string m_PRWLumiCalcFiles_string;
#endif

         

    };
}
#endif
