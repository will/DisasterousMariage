#ifndef DisasterousMariage_AnalysisUtils_H
#define DisasterousMariage_AnalysisUtils_H

#include <xAODBase/IParticleHelpers.h>

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/TauJetContainer.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthVertex.h"

#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODBTagging/BTagging.h"

#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingET.h"
#include <vector>
#include "PATInterfaces/SystematicSet.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "TLorentzVector.h"
#include <TMath.h>
#include <iostream>
#include <fstream>

#include <AsgTools/AsgTool.h>
#include <AsgTools/ToolHandle.h>

namespace MariageOfDisaster {
    std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &Files);
    void FillVectorFromString(std::vector<std::string> & str_vector, std::string &str);
    std::string EraseWhiteSpaces(std::string str);
    std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep);
    bool GetLine(std::ifstream &inf, std::string &line);
    std::string RemoveAllExpInStr(std::string Str, std::string Exp);
    template<typename T> bool IsInVector(const T& Ele, const std::vector<T> &Vec) {
        for (auto& In : Vec) {
            if (Ele == In) return true;
        }
        return false;
    }
}
#endif
